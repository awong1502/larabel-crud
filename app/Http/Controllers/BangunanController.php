<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bangunan;
use Illuminate\Support\Facades\Auth;

class BangunanController extends Controller
{
    public function inputView(){
        return view('input.index');
    }

    public function read(){
        $data = Bangunan::all();
        
        return view('index', ['bangunan' => $data]);
    }

    public function inputCreate(Request $request){
        $bangunan = new Bangunan();
        $bangunan->user_id = Auth::user()->id;
        $bangunan->rumah = $request->nama_rumah;
        $bangunan->hotel = $request->nama_hotel;
        $bangunan->save();
        return redirect()->back();
    }
}
