<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class productController extends Controller
{
    public function productCreateView(){
        return view('product.post');
    }

    public function productView(){
        $data = Product::all();

        return view('product.index', ['product' => $data]);
    }

    public function productCreate(Request $Request){
        $product = new product();
        $product->user_id = Auth::user()->id;
        $product->nama_produk = $Request->nama_produk;
        $product->stok = $Request->stok;
        $product->save();
        return redirect()->back();
    }

    public function edit(Project $project)
    {
        return view('product.edit', compact('product'));
    }

    public function update(Request $request, Project $product)
    {
        $request->validate([
            'nama_produk' => 'required',
            'stok' => 'required'
        ]);
        $product->update($request->all());

        return redirect()->route('product.index')
            ->with('success', 'Project updated successfully');
    }

    public function destroy(Project $project)
    {
        $project->delete();

        return redirect()->route('projects.index')
            ->with('success', 'Project deleted successfully');
    }

    public function productEditView($id){
        return view('product.edit');
    }

    public function productEditViewData($id){
        $data = product::where('id',$id)->first();
        
        return view('product.edit', ['product' => $data]);
    }

    public function productEditViewDataUpdate(Request $Request , $id){
        $updateProduct = product::where('id',$id)->first();
        $updateProduct->nama_produk = $Request->nama_produk;
        $updateProduct->stok = $Request->stok;
        $updateProduct->update();
        return redirect()->back();

    }

    public function delete($id){
        $data = product::where('id', $id)->first();

        $data->delete();
        return redirect()->back();
    }
    
}
