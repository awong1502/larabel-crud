@extends('layouts.app')

@section('content')
    <div class="container col-md-4">
            
        <form method="post" action="/product/edit/{{ $product->id }}">
            @csrf
            @method('put')
            <div class="form-group">
            <label for="exampleInputEmail1">Nama Produk</label>
            <input type="text" class="form-control" name="nama_produk" value="{{ $product->nama_produk }}">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
            <label for="exampleInputPassword1">Stock</label>
            <input type="number" class="form-control" name="stok" value="{{$product->stok}}">
            </div>
            <div class="form-group form-check">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    
@endsection