@extends('layouts.app')

@section('content')

<div class="container">
    <center>
        <h1>Table Product</h1>
    </center>

    <table class="table">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama Product</th>
                <th>Stock</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php $x = 1?>
                @foreach ($product as $d)
                <td scope="row">{{$x}}</td>
                <td>{{ $d->nama_produk }}</td>
                <td>{{ $d->stok }}</td>
                <td><a href="/product/edit/{{$d->id}}">Edit</a></td>
                <td><a href="/product/delete/{{$d->id}}">Delete</a></td>
            </tr>
            
            <?php $x++?> 
            @endforeach
        </tbody>
    </table>
</div>
@endsection