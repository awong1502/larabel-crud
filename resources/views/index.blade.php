@extends('layouts.app')

@section('content')

<div class="container">
    <center>
        <h1>Table Bangunan</h1>
    </center>

    <table class="table">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama Rumah</th>
                <th>Nama Hotel</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                @foreach ($bangunan as $d)
                <td scope="row">{{ $d->user_id }}</td>
                <td>{{ $d->rumah }}</td>
                <td>{{ $d->hotel }}</td>
            </tr>
            <tr>
                <td scope="row"></td>
                <td></td>
                <td></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection