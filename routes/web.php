<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BangunanController;
use App\Http\Controllers\productController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', [BangunanController::class, 'read']);

Route::post('/input', [BangunanController::class, 'inputCreate']);

Route::get('/input', [BangunanController::class, 'inputView']);

Route::get('/product', [productController::class, 'productView']);

Route::get('/product/input', [productController::class, 'productCreateView']);

Route::post('/product/input', [productController::class, 'productCreate']);

Route::get('/product/edit/{id}', [productController::class, 'productEditView']);

Route::get('/product/edit/{id}', [productController::class, 'productEditViewData']);

Route::put('/product/edit/{id}', [productController::class, 'productEditViewDataUpdate']);

Route::get('/product/delete/{id}', [productController::class, 'delete']);


