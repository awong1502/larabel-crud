@extends('layouts.app')

@section('content')
    <div class="container col-md-4 mt-5">
        <div class="form-group">
            <form action="/input" method="post">
                @csrf
                @method('post')
            <label for="">Nama Rumah</label>
            <input type="text" name="nama_rumah" id="" class="form-control" placeholder="" aria-describedby="helpId">
            <small id="helpId" class="text-muted">Input Nama Rumah</small><br><br>
            <label for="">Nama Hotel</label>
            <input type="text" name="nama_hotel" id="" class="form-control" placeholder="" aria-describedby="helpId">
            <small id="helpId" class="text-muted">Input Nama Hotel</small><br>
            <button class="btn btn-primary" type="submit">Submit</button>
        </form>
        </div>
    </div>
@endsection
