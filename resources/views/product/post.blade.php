@extends('layouts.app')

@section('content')
    <div class="container col-md-4 mt-5">
        <div class="form-group">
            <form action="/product/input" method="post">
                @csrf
                @method('post')
            <label for="">Nama Product</label>
            <input type="text" name="nama_produk" id="" class="form-control" placeholder="" aria-describedby="helpId">
            <small id="helpId" class="text-muted">Input Nama Rumah</small><br><br>
            <label for="">Stock</label>
            <input type="text" name="stok" id="" class="form-control" placeholder="" aria-describedby="helpId">
            <small id="helpId" class="text-muted">Input Nama Hotel</small><br>
            <button class="btn btn-primary" type="submit">Submit</button>
        </form>
        </div>
    </div>
@endsection
